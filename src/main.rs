use std::{
  env, fs,
  fs::File,
  io,
  path::{Path, PathBuf},
};

mod convert;

fn main() {
  let (input, output) = match read_args() {
    Some(v) => v,
    None => {
      println!("Usage: px2svg <input> <output>");
      return;
    }
  };
  if !input.exists() {
    println!("The input `{}` does not exist!", input.display());
    return;
  }
  if !output.exists() {
    println!("The output `{}` does not exist!", output.display());
    return;
  }
  match (input.is_dir(), output.is_dir()) {
    (true, true) => match read_dir(&input, &output, &Path::new("")) {
      Ok(_) => (),
      Err(e) => println!("Error reading `{}`: {}", input.display(), e),
    },
    (true, false) => {
      println!("Cannot parse an input directory to an output file");
      return;
    }
    (false, true) => read_file(&input, &output),
    (false, false) => read_file(&input, &output),
  }
}

/// Input and output are the command args. Dir is the directory this should
/// search. That will be a directory relative to input.
fn read_dir(input: &Path, output: &Path, dir: &Path) -> io::Result<()> {
  for f in input.join(dir).read_dir()? {
    let f = f?;
    let p = f.path();
    let rel = p.strip_prefix(input).unwrap();
    if f.metadata()?.is_dir() {
      fs::create_dir_all(output.join(&rel))?;
      read_dir(input, output, &rel)?;
    } else {
      let name = f.file_name();
      let mut name = name.to_str().unwrap();
      if name.ends_with(".png") {
        name = name.strip_suffix(".png").unwrap();
        convert::convert(
          &input.join(rel),
          &output.join(rel.parent().unwrap()).join(format!("{}.svg", name)),
        );
      }
    }
  }
  Ok(())
}
fn read_file(input: &Path, output: &Path) {
  if output.is_dir() {
    let name = input.file_name().unwrap();
    let mut name = name.to_str().unwrap();
    name = name.strip_suffix(".png").unwrap();
    convert::convert(input, &output.join(format!("{}.svg", name)));
  } else {
    convert::convert(input, output);
  }
}

fn read_args() -> Option<(PathBuf, PathBuf)> {
  let mut args = env::args();
  args.next().unwrap();
  let res = (args.next()?.into(), args.next()?.into());
  if args.next().is_some() {
    None
  } else {
    Some(res)
  }
}
