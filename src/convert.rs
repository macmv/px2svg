use image::GenericImageView;
use std::{fs::File, path::Path};
use svg::{
  node::element::{path::Data, Path as SvgPath},
  Document,
};

const MAX_WIDTH: u32 = 512;
const MAX_HEIGHT: u32 = 512;
const OVERLAP: f32 = 0.05;

pub fn convert(input: &Path, output: &Path) {
  println!("Converting `{}`...", input.display());
  let im = match image::open(input) {
    Ok(v) => v,
    Err(e) => {
      println!("Error reading `{}`: {}", input.display(), e);
      return;
    }
  };

  if im.width() > MAX_WIDTH || im.height() > MAX_HEIGHT {
    println!(
      "Skipping `{}`, because the image is too large ({} x {})",
      input.display(),
      im.width(),
      im.height()
    );
    return;
  }

  let mut document = Document::new().set("viewBox", (0, 0, im.width(), im.height()));
  for x in 0..im.width() {
    for y in 0..im.height() {
      let px = im.get_pixel(x, y);
      // Skip full alpha pixels
      if px[3] == 0 {
        continue;
      }
      let data = Data::new()
        .move_to((x, y))
        .line_by((0, 1.0 + OVERLAP))
        .line_by((1.0 + OVERLAP, 0))
        .line_by((0, -1.0 - OVERLAP))
        .close();
      let path = SvgPath::new()
        .set("fill", format!("#{:02x}{:02x}{:02x}", px[0], px[1], px[2]))
        .set("d", data);
      document = document.add(path);
    }
  }

  match svg::save(output, &document) {
    Ok(_) => (),
    Err(e) => {
      println!("Error saving `{}`: {}", output.display(), e);
    }
  }
}
